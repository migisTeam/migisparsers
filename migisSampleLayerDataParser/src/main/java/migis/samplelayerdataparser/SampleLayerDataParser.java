/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migis.samplelayerdataparser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import migis.samplelayerdataparser.CommonLogic.LayerData;
import migis.samplelayerdataparser.CommonLogic.LayerData.Region;

/**
 *
 * @author Re
 */
public class SampleLayerDataParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        try {
            LayerData layerData = new LayerData(41.121661, 55.422261, 46.427442, 87.296582, 1500, 3000);
            Region region = layerData.region;

            for (int row=0;row<region.latRes;row++) {
                ArrayList<Double> layerRow = layerData.data.get(row);
                for (int col=0;col<region.lonRes;col++) {
                    Double value = (double)(col+row)/(region.latRes+region.lonRes);
                    layerRow.set(col, value);
                }
            }
            
            CommonLogic.saveResult(layerData, "layerData.json");
        }
        catch (Exception e){
            int a=1;
        }
        
    }
    
}

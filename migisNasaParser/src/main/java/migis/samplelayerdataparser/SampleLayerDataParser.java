/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migis.samplelayerdataparser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import flanagan.interpolation.BiCubicSpline;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import migis.samplelayerdataparser.CommonLogic.LayerData;
import migis.samplelayerdataparser.CommonLogic.LayerData.Region;

/**
 *
 * @author Re
 */
public class SampleLayerDataParser {

    static double[] x = new double[180];
    static double[] y = new double[360];
    static double[][] data = new double[180][360];
    static BiCubicSpline inter;

    static final String data_path = "C:\\Users\\Shafhat\\Downloads\\Data";
    
  /*  static void generatePNG(String fileName) throws IOException {
        BufferedImage image = new BufferedImage(3600, 1800, BufferedImage.TYPE_INT_RGB);
        double min = getMin();
        double max = getMax();

        ArrayList<Color> colors = new ArrayList<>();
        colors.add(new Color(255, 255, 224));
        colors.add(new Color(255, 203, 145));
        colors.add(new Color(254, 144, 106));
        colors.add(new Color(231, 87, 88));
        colors.add(new Color(192, 34, 59));
        colors.add(new Color(139, 0, 0));

        for (int i = 0; i < 1800; i++) {
            for (int j = 0; j < 3600; j++) {
                double val = data[i / 10][j / 10];
                val = ((val - min) * (255 / (max - min)));
                if (val > 255) {
                    val = 255;
                }
                if (val < 0) {
                    val = 0;
                }
                //Color c = new Color((int) val, 0, 0); //Continious red-scale
                Color c = colors.get((int) (((val / 255 - 0.01)) * colors.size())); //discrete color scale
                image.setRGB(j, i, c.getRGB());
            }
        }
        File outputfile = new File("C:\\Users\\Shafhat\\Downloads\\Data\\" + fileName + "_original.png");
        ImageIO.write(image, "png", outputfile);

        BufferedImage image2 = new BufferedImage(3600, 1800, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < 1800; i++) {
            for (int j = 0; j < 3600; j++) {
                double x1 = i / 10.0 - 90;
                double y1 = j / 10.0 - 180;
                if (x1 > 89) {
                    x1 = 89;
                }
                if (y1 > 179) {
                    y1 = 179;
                }
                double val = inter.interpolate(x1, y1);
                val = ((val - min) * (255 / (max - min)));
                if (val > 255) {
                    val = 255;
                }
                if (val < 0) {
                    val = 0;
                }
                //Color c = new Color((int) val, 0, 0); //Continious red-scale
                Color c = colors.get((int) (((val / 255 - 0.01)) * colors.size()));  //discrete color scale
                image2.setRGB(j, i, c.getRGB());
            }
        }
        File outputfile2 = new File("C:\\Users\\Shafhat\\Downloads\\Data\\" + fileName + "_inter.png");
        ImageIO.write(image2, "png", outputfile2);
    } */


    static double getMax() {
        double max = -1;
        for (double[] darr : data) {
            for (double d : darr) {
                if (d > max) {
                    max = d;
                }
            }
        }
        return max;
    }

    static double getMin() {
        double min = Double.MAX_VALUE;
        for (double[] darr : data) {
            for (double d : darr) {
                if (d < min) {
                    min = d;
                }
            }
        }
        return min;
    }

    static void init(String filename) throws FileNotFoundException, IOException {

        for (int i = 0; i < 180; i++) {
            x[i] = -90.0 + i;
        }
        for (int i = 0; i < 360; i++) {
            y[i] = -180.0 + i;
        }
        try (BufferedReader br = new BufferedReader(new FileReader(data_path + "\\" + filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                int x_i = Integer.parseInt(line.split("\t")[0]) + 90;
                int y_i = Integer.parseInt(line.split("\t")[1]) + 180;
                data[x_i][y_i] = Double.parseDouble(line.split("\t")[2]);
            }
        }
    }

    /**
     * saldkjflskjdfl
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        try {
            LayerData layerData = new LayerData(41.121661, 55.422261, 46.427442, 87.296582, 1500, 3000);
            Region region = layerData.region;

            String filename = "10yr_ws50_10_14";
            init(filename);
            inter = new BiCubicSpline(x, y, data);
            //generatePNG(filename);
            Double stepLat = (layerData.region.latTo - layerData.region.latFrom) / layerData.region.latRes;
            Double stepLon = (layerData.region.lonTo - layerData.region.lonFrom) / layerData.region.lonRes;
            Double max = getMax();
            Double min = getMin();

            for (int row = 0; row < region.latRes; row++) {
                ArrayList<Double> layerRow = layerData.data.get(row);
                for (int col = 0; col < region.lonRes; col++) {
                    Double res = inter.interpolate(region.latFrom + row * stepLat, region.lonFrom + col * stepLon);

                    //Integer value = (int) (((res - min) / (max - min)) * 100);
                    Double value = (double) (res);
                    layerRow.set(col, value);
                }
            }

            CommonLogic.saveResult(layerData, "C:\\Users\\Shafhat\\Downloads\\Data\\10yr_ws50_10_14_Data.json");
        } catch (Exception e) {
        }
    }
}

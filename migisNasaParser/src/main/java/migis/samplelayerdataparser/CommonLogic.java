/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migis.samplelayerdataparser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Re
 */
public class CommonLogic {
    public static class LayerData {
        public static class Region {
            public Double latFrom;
            public Double latTo;        
            public Double lonFrom;    
            public Double lonTo;            
            public Integer latRes;
            public Integer lonRes;           
        }
        public Region region;
        public ArrayList<ArrayList<Double>> data;    
        
        public LayerData(Double latFrom, Double latTo, Double lonFrom, Double lonTo, Integer latRes, Integer lonRes) {
            region = new Region();
            region.latFrom = latFrom;
            region.latTo = latTo;
            region.lonFrom = lonFrom;
            region.lonTo = lonTo;
            region.latRes = latRes;
            region.lonRes = lonRes;
            data = new ArrayList<>();
            for (int row=0;row<region.latRes;row++) {
                ArrayList<Double> layerRow = new ArrayList<>();
                data.add(layerRow);
                for (int col=0;col<region.lonRes;col++) {
                    layerRow.add(null);
                }
            }
        }
    }    
    
    public static void saveResult(LayerData layerData, String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        
        for (ArrayList<Double> row : layerData.data) {
            for (int i=0;i<row.size();i++) {
                BigDecimal bd = new BigDecimal(row.get(i)).setScale(5, RoundingMode.FLOOR);
                row.set(i, bd.doubleValue());
            }
        }
        
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.valueToTree(layerData);
        String strResult = node.toString();        
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        writer.write(strResult);
        writer.close();         
    }    
}
